use core::arch::asm;

use tx1_a57_common::{
    hardware::tmr::full_system_reset_via_watchdog,
    static_panic,
    utils::{colour_to_scratch_contents, set_pmc_scratch_200_if_unset},
};

use crate::hardware::mmu;

#[no_mangle]
#[link_section = ".vectors.current_el.sp_el0.synchronous"]
#[naked]
pub extern "C" fn vt_handle_synchronous_interrupt_from_current_el_using_sp_el0() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_el0.irq"]
#[naked]
pub extern "C" fn vt_handle_irq_from_current_el_using_sp_el0() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_el0.fiq"]
#[naked]
pub extern "C" fn vt_handle_fiq_from_current_el_using_sp_el0() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_el0.serror"]
#[naked]
pub extern "C" fn vt_handle_serror_from_current_el_using_sp_el0() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_elx.synchronous"]
#[naked]
pub extern "C" fn vt_handle_synchronous_interrupt_from_current_el_using_sp_elx() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_elx.irq"]
#[naked]
pub extern "C" fn vt_handle_irq_from_current_el_using_sp_elx() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_elx.fiq"]
#[naked]
pub extern "C" fn vt_handle_fiq_from_current_el_using_sp_elx() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.current_el.sp_elx.serror"]
#[naked]
pub extern "C" fn vt_handle_serror_from_current_el_using_sp_elx() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a64.synchronous"]
#[naked]
pub extern "C" fn vt_handle_synchronous_interrupt_from_lower_el_a64() {
    unsafe {
        asm!(
            "b #handle_synchronous_interrupt_from_lower_el_a64",
            options(noreturn)
        );
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a64.irq"]
#[naked]
pub extern "C" fn vt_handle_irq_from_lower_el_a64() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a64.fiq"]
#[naked]
pub extern "C" fn vt_handle_fiq_from_lower_el_a64() {
    unsafe {
        asm!(
            "b #vt_handle_generic",
            "stp x0, x1, [sp, #-0x10]!",
            "mrs x1, s3_0_c0_c0_5",
            "and x1, x1, #0x3",
            "cmp x1, #0x3",
            "ldp x0, x1, [sp], #0x10",
            "b.ne .",
            "eret",
            options(noreturn)
        )
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a64.serror"]
#[naked]
pub extern "C" fn vt_handle_serror_from_lower_el_a64() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a32.synchronous"]
#[naked]
pub extern "C" fn vt_handle_synchronous_interrupt_from_lower_el_a32() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a32.irq"]
#[naked]
pub extern "C" fn vt_handle_irq_from_lower_el_a32() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a32.fiq"]
#[naked]
pub extern "C" fn vt_handle_fiq_from_lower_el_a32() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
#[link_section = ".vectors.lower_el.a32.serror"]
#[naked]
pub extern "C" fn vt_handle_serror_from_lower_el_a32() {
    unsafe {
        asm!("b #vt_handle_generic", options(noreturn));
    }
}

#[no_mangle]
pub extern "C" fn vt_handle_generic() -> ! {
    set_pmc_scratch_200_if_unset(
        mmu::PMC_BASE,
        colour_to_scratch_contents(0xFF, 0xAA, 0xBB, 0x10),
    );
    full_system_reset_via_watchdog(mmu::TMR_BASE);
}

#[no_mangle]
pub extern "C" fn vt_handle_panic() -> ! {
    set_pmc_scratch_200_if_unset(
        mmu::PMC_BASE,
        colour_to_scratch_contents(0xFF, 0x00, 0x00, 0x10),
    );
    full_system_reset_via_watchdog(mmu::TMR_BASE);
}

#[no_mangle]
#[naked]
pub extern "C" fn write_sp_to_mbx() {
    unsafe {
        asm!(
            "MOV X0, #0x1f0000000",
            "ORR X0, X0, #0x60000",
            "MOV X1, SP",
            "STR X1, [X0]",
            "ADD X0, X0, #8",
            "MRS X1, ESR_EL3",
            "STR X1, [X0]",
            "RET",
            options(noreturn)
        )
    }
}

#[no_mangle]
#[naked]
pub extern "C" fn handle_synchronous_interrupt_from_lower_el_a64() -> ! {
    unsafe {
        asm!(
            "stp x0, x1, [sp, #-0x10]!",
            "mrs x1, esr_el3",
            "lsr w0, w1, #26",
            "cmp w0, 0b010111",
            "ldp x0, x1, [sp], #0x10",
            "b.ne #vt_handle_generic", // If exception is not an SMC from A64 whilst SMCs are enabled then do a full system reset via WDs
            "stp x29, x30, [sp, #-0x10]!",
            "stp x18, x19, [sp, #-0x10]!",
            "stp x16, x17, [sp, #-0x10]!",
            "stp x14, x15, [sp, #-0x10]!",
            "stp x12, x13, [sp, #-0x10]!",
            "stp x10, x11, [sp, #-0x10]!",
            "stp x8, x9, [sp, #-0x10]!",
            "stp x6, x7, [sp, #-0x10]!",
            "stp x4, x5, [sp, #-0x10]!",
            "stp x2, x3, [sp, #-0x10]!",
            "stp x0, x1, [sp, #-0x10]!", // This makes our [PushedRegisters] structure on the stack
            "mov x1, sp",
            "mrs x0, esr_el3",
            "and x0, x0, #0xffff",
            "bl #decode_smc",
            "ldp x0, x1, [sp], #0x10",
            "ldp x2, x3, [sp], #0x10",
            "ldp x4, x5, [sp], #0x10",
            "ldp x6, x7, [sp], #0x10",
            "ldp x8, x9, [sp], #0x10",
            "ldp x10, x11, [sp], #0x10",
            "ldp x12, x13, [sp], #0x10",
            "ldp x14, x15, [sp], #0x10",
            "ldp x16, x17, [sp], #0x10",
            "ldp x18, x19, [sp], #0x10",
            "ldp x29, x30, [sp], #0x10",
            "eret",
            options(noreturn)
        );
    }
}

#[inline(always)]
pub fn setup_vector_table() {
    extern "C" {
        static __vectors_start: u8;
        static __vectors_end: u8;
    }

    unsafe {
        let start_ptr = &__vectors_start as *const u8 as u64;
        let end_ptr = &__vectors_end as *const u8 as u64;

        match (end_ptr - start_ptr).cmp(&0x800) {
            core::cmp::Ordering::Less => static_panic!(mmu::UART_BASE, "Vector table too small"),
            core::cmp::Ordering::Equal => {}
            core::cmp::Ordering::Greater => static_panic!(mmu::UART_BASE, "Vector table too large"),
        }

        asm!(
            "MSR vbar_el3, {vbar}",
            vbar = in(reg) start_ptr
        );
    }
}

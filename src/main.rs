#![no_std]
#![no_main]
#![feature(naked_functions)]
#![feature(panic_info_message)]
#![allow(clippy::needless_range_loop)]

use hardware::{
    mailbox::{BOOT_STATE, MAILBOX_BASE, SECMON_STATE},
    mmu,
};
use tx1_a57_common::{
    hardware::security_engine::panic_if_se_isnt_idle,
    log,
    mmio::{self, mmio_or, mmio_read, mmio_write},
    utils::usleep,
};

use crate::runtime::route_external_aborts_and_serrors_to_el3;

pub mod hardware;
pub mod pkg2;
pub mod runtime;
pub mod smc;
pub mod vectors;

fn main() -> ! {
    log::write(mmu::UART_BASE, "[Nxyoom] Booting\n");
    mmio_write(MAILBOX_BASE, SECMON_STATE, 1);

    while mmio_read(MAILBOX_BASE, BOOT_STATE) != 2 {
        usleep(mmu::TMR_BASE, 1);
    }

    panic_if_se_isnt_idle(mmu::SE_BASE, mmu::UART_BASE);
    hardware::gic400::setup(mmu::INTERRUPT_DISTRIBUTOR_BASE);

    hardware::security_engine::initial_setup(
        mmu::SE_BASE,
        mmu::FUSE_BASE,
        mmu::CAR_BASE,
        mmu::PMC_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
    );

    log::write(mmu::UART_BASE, "[Nxyoom] Security Engine initialised\n");

    // TMR5 secure mode config
    // TMR6 secure mode config
    // TMR7 secure mode config
    // TMR8 secure mode config
    // WD0 secure mode config
    // WD1 secure mode config
    // WD2 secure mode config
    // WD3 secure mode config
    mmio_write(mmu::TMR_BASE, mmio::tmr::SHARED_TIMER_SECURE_CFG, 0xf1e0);

    // Active cluster lock
    mmio_write(
        mmu::FLOW_CONTROLLER_BASE,
        mmio::flow_controller::FLOW_CTLR_BPMP_CLUSTER_CONTROL,
        4,
    );
    // FIQ2CCPLEX enable
    mmio_write(
        mmu::FLOW_CONTROLLER_BASE,
        mmio::flow_controller::FLOW_CTLR_FLOW_DBG_QUAL,
        1 << 28,
    );

    // Clear deep sleep cycle bit
    mmio_write(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_DPD_ENABLE, 0);

    hardware::memory_controller::setup_carveouts(mmu::MEMORY_CONTROLLER_BASE);
    log::write(mmu::UART_BASE, "[Nxyoom] Carveouts setup\n");

    mmio_write(
        mmu::APB_BASE,
        mmio::apb::APB_MISC_SECURE_REGS_APB_SLAVE_SECURITY_ENABLE_REG0,
        0x504244,
    );

    mmio_write(
        mmu::APB_BASE,
        mmio::apb::APB_MISC_SECURE_REGS_APB_SLAVE_SECURITY_ENABLE_REG1,
        0xa3700000,
    );

    mmio_write(
        mmu::APB_BASE,
        mmio::apb::APB_MISC_SECURE_REGS_APB_SLAVE_SECURITY_ENABLE_REG2,
        0x304,
    );

    hardware::memory_controller::setup_smmu(mmu::SE_BASE);
    log::write(mmu::UART_BASE, "[Nxyoom] SMMU setup\n");

    runtime::setup_reset_vector();
    log::write(mmu::UART_BASE, "[Nxyoom] Reset Vector written\n");

    hardware::gic400::gicd_set_prio(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x5A, 0);
    hardware::gic400::gicd_set_irq_group(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x5A, false);
    hardware::gic400::gicd_set_irq_enb(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x5A);
    hardware::gic400::gicd_set_target_cpu(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x5A, 8);
    hardware::gic400::gicd_set_cfg(
        mmu::INTERRUPT_DISTRIBUTOR_BASE,
        0x5A,
        mmio::gic400::GicdCfg::LevelSensitive,
    );

    log::write(mmu::UART_BASE, "[Nxyoom] GICD Setup\n");

    runtime::per_core_setup();

    let dec_header = pkg2::parse_pkg2_header();
    log::write(mmu::UART_BASE, "[Nxyoom] Decrypted PKG2 header\n");

    if let Err(e) = dec_header.validate() {
        e.panic()
    }

    log::write(mmu::UART_BASE, "[Nxyoom] Validated PKG2 header\n");

    let output_res = dec_header.unpack();
    log::write(mmu::UART_BASE, "[Nxyoom] Unpacked PKG2\n");

    hardware::cpu::set_core_state(0, output_res.section0_addr, 0);

    mmio_or(
        mmu::APB_BASE,
        mmio::apb::APB_MISC_SECURE_REGS_APB_SLAVE_SECURITY_ENABLE_REG0,
        mmio::apb::APB_MISC_SECURE_REGS_APB_SLAVE_SECURITY_ENABLE_PMC,
    );

    hardware::mmu::unmap_lvl1_blocks();

    route_external_aborts_and_serrors_to_el3(false);
    log::write(mmu::UART_BASE, "[Nxyoom] Dropping to EL1\n");
    runtime::drop_to_el1();
}

use tx1_a57_common::mmio::{self, mmio_read};

pub mod cpu;
pub mod fuse;
pub mod gic400;
pub mod mailbox;
pub mod memory_controller;
pub mod mmu;
pub mod security_engine;

pub fn get_device_id() -> u64 {
    let fab_code = mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_OPT_FAB_CODE);
    let lot_code = mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_OPT_LOT_CODE_0);
    let wafer_id = mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_OPT_WAFER_ID);
    let x_coord = mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_OPT_X_COORDINATE);
    let y_coord = mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_OPT_Y_COORDINATE);
    let mut x19 = 0;
    let mut shift = 0x18;
    for _ in 0..5 {
        let shifted_lot_code = lot_code >> shift;
        shift -= 6;
        x19 = (shifted_lot_code & 0x3f) + x19 * 0x24;
    }

    let mut out = 0u64;
    out |= (y_coord & 0b1) as u64;
    out |= (((((x_coord & 0x1ff) << 1) as u8 & 0xfe) | ((y_coord & 0x100) >> 8) as u8) as u64) << 8;
    out |= (((x_coord >> 7) & 3) as u64) << (8 * 2);

    let res0 = do_thing(&mut out, 0x12, 6, wafer_id & 0x3f) as u32 + 0x12;
    let res1 = do_thing(&mut out, res0 as u32, 0x1a, x19) as u32;
    do_thing(&mut out, res0 + res1, 6, fab_code & 0x3f);
    out
}

fn do_thing(inp: &mut u64, byte: u32, shift: u32, arg4: u32) -> u64 {
    let shifted = 1u32 << shift as u32;
    let adjusted_byte = match (byte as i32) < 0 {
        true => byte + 7,
        false => byte,
    };

    let shifted_input_byte = adjusted_byte >> 3;
    let masked_arg4 = (shifted - 1) & arg4;
    let diff = byte - (adjusted_byte & 0xfffffff8);
    let new_val = shift + diff + 7;
    *inp |= (((masked_arg4 << diff) << (shifted_input_byte * 8)) & 0xFF) as u64;
    if new_val as i32 >= 0x10 {
        let adjusted_new_val = match (new_val as i32) < 0 {
            true => new_val + 7,
            false => new_val,
        };

        let shifted_new_val = adjusted_new_val as u64 >> 3;
        let mut new_shift = 8 - diff;
        for iter in 1..shifted_new_val {
            let x13_1 = (masked_arg4 >> new_shift) as u8;
            new_shift += 8;
            *inp |= (x13_1 as u64) << (iter * 8);
        }
    }
    shift as u64
}

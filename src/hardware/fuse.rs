use tx1_a57_common::mmio::mmio_write;

pub fn disable_regprogram(base: usize) {
    mmio_write(base, tx1_a57_common::mmio::fuse::DISABLEREGPROGRAM, 1);
}

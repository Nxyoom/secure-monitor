use spin::Mutex;
use tx1_a57_common::{
    dsb_ish,
    hardware::{
        clock_and_reset::set_misc_clk_enb,
        security_engine::{
            aes::{self, aes_operation, set_aes_keyslot_perms, unwrap_key},
            rng::{self, generate_random},
            rsa, set_security_state,
        },
    },
    mmio::{self, mmio_read, mmio_write, security_engine},
    static_panic,
    utils::{
        align_buffer, clear_and_invalidate_cache_for_address_range, set_pmc_scratch_200_if_unset,
    },
    verify_alignment,
};

pub fn some_keygen(se_base: usize, tmr_base: usize, uart_base: usize, buffer: &mut [u8]) {
    if buffer.len() != 0x10 {
        static_panic!(uart_base, "Buffer len != 0x10 in some_keygen");
    }

    verify_alignment!(uart_base, buffer.as_ptr(), "some_keygen");

    for x in 0..0x10 {
        buffer[x] = 0;
    }

    let mut temp = [0u8; 0x100];
    let aligned_temp = align_buffer(&mut temp, 0x40, 0x10);

    verify_alignment!(uart_base, aligned_temp.as_ptr(), "some_keygen 2");

    clear_and_invalidate_cache_for_address_range(buffer.as_ptr() as u64, 0x10);
    dsb_ish!();
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xa,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xc,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xd,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xa,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xc,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);

    aes_operation(
        se_base,
        tmr_base,
        uart_base,
        aligned_temp,
        buffer,
        0x202,
        0xd,
    );
    clear_and_invalidate_cache_for_address_range(aligned_temp.as_ptr() as u64, 0x10);
    dsb_ish!();
    buffer.copy_from_slice(aligned_temp);
}

struct SecurityState {
    pub security: u8,
    pub tzram_security: u8,
    pub crypto_security_perkey: u16,
    pub crypto_keytable_access: [u8; 0x10],
    pub rsa_security_perkey: u8,
    pub rsa_keytable_access: [u8; 2],
}

const STARTUP_STATE: SecurityState = SecurityState {
    security: 1,
    tzram_security: 0,
    crypto_security_perkey: 0,
    crypto_keytable_access: [
        0x6a, 0x6a, 0x6a, 0x6a, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0x2a, 0x00, 0x2a, 0x00, 0x00, 0x6a,
        0x6a,
    ],
    rsa_security_perkey: 0x00,
    rsa_keytable_access: [0x06, 0x02],
};

pub fn validate_security_state(base: usize) -> bool {
    (mmio_read(base, security_engine::SECURITY_CONTROL) & 0xFFFF) == STARTUP_STATE.security as u32
        && (mmio_read(base, security_engine::TZRAM_SECURITY) & 0xFFFF)
            == STARTUP_STATE.tzram_security as u32
        && (mmio_read(base, security_engine::SECURITY_CONTROL) & 0xFFFF)
            == STARTUP_STATE.security as u32
        && (mmio_read(base, security_engine::CRYPTO_SECURITY_PERKEY) & 0xFFFF)
            == STARTUP_STATE.crypto_security_perkey as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_0) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[0] as u32
        // && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_1) & 0xFFFF)
            // == STARTUP_STATE.crypto_keytable_access[1] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_2) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[2] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_3) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[3] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_4) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[4] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_5) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[5] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_6) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[6] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_7) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[7] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_8) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[8] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_9) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[9] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_10) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[10] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_11) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[11] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_12) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[12] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_13) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[13] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_14) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[14] as u32
        && (mmio_read(base, security_engine::CRYPTO_KEYTABLE_ACCESS_15) & 0xFFFF)
            == STARTUP_STATE.crypto_keytable_access[15] as u32
        && (mmio_read(base, security_engine::RSA_SECURITY_PERKEY) & 0xFFFF)
            == STARTUP_STATE.rsa_security_perkey as u32
        && (mmio_read(base, security_engine::RSA_KEYTABLE_ACCESS_0) & 0xFFFF)
            == STARTUP_STATE.rsa_keytable_access[0] as u32
    // && (mmio_read(base, security_engine::RSA_KEYTABLE_ACCESS_1) & 0xFFFF)
    // == STARTUP_STATE.rsa_keytable_access[1] as u32
}

pub fn setup_rng_and_keyslots(se_base: usize, tmr_base: usize, uart_base: usize) {
    rng::setup_rng(se_base, tmr_base, uart_base);

    let mut ua_out = [0u8; 0x100];
    let out = align_buffer(&mut ua_out, 0x40, 0x10);

    clear_and_invalidate_cache_for_address_range(out.as_ptr() as u64, 0x10);
    dsb_ish!();
    rng::generate_random(se_base, tmr_base, uart_base, out);
    dsb_ish!();
    clear_and_invalidate_cache_for_address_range(out.as_ptr() as u64, 0x10);
    dsb_ish!();

    unwrap_key(se_base, tmr_base, uart_base, 0xa, 0xd, out);
    set_aes_keyslot_perms(se_base, uart_base, 0xa, 0xff);
    for ks in 4..0xe {
        set_aes_keyslot_perms(se_base, uart_base, ks, 0x40);
    }
    set_aes_keyslot_perms(se_base, uart_base, 1, 0x40);
}

#[repr(align(0x40))]
pub struct RngBuffer(pub [u8; 0x100], pub usize);

pub static RNG_BUFFER: Mutex<RngBuffer> = Mutex::new(RngBuffer([0u8; 0x100], 0));

pub fn get_from_rng_buffer(bytes: &mut [u8]) -> bool {
    let mut guard = RNG_BUFFER.lock();

    if guard.1 < bytes.len() {
        return false;
    }

    bytes.copy_from_slice(&guard.0[guard.1..(guard.1 + bytes.len())]);
    guard.1 -= bytes.len();

    true
}

pub fn refill_rng_buffer(se_base: usize, tmr_base: usize, uart_base: usize, spin: bool) -> bool {
    let mut guard = match spin {
        true => RNG_BUFFER.lock(),
        false => match RNG_BUFFER.try_lock() {
            Some(guard) => guard,
            None => {
                return false;
            }
        },
    };

    if guard.1 != 0x100 {
        clear_and_invalidate_cache_for_address_range(guard.0.as_ptr() as u64, 0x40);
        dsb_ish!();
        generate_random(se_base, tmr_base, uart_base, &mut guard.0);
        dsb_ish!();
        clear_and_invalidate_cache_for_address_range(guard.0.as_ptr() as u64, 0x40);
        dsb_ish!();
        guard.1 = 0x100;
    }

    true
}

pub fn initial_setup(
    se_base: usize,
    fuse_base: usize,
    car_base: usize,
    pmc_base: usize,
    tmr_base: usize,
    uart_base: usize,
) {
    setup_rng_and_keyslots(se_base, tmr_base, uart_base);

    let mut buffer = [0u8; 0x100];
    let aligned_buffer = align_buffer(&mut buffer, 0x80, 0x10);
    some_keygen(se_base, tmr_base, uart_base, aligned_buffer);

    set_misc_clk_enb(car_base, true);
    mmio_write(fuse_base, mmio::fuse::PRIVATEKEYDISABLE, 0x10);
    set_security_state(se_base, true);

    if !validate_security_state(se_base) {
        set_pmc_scratch_200_if_unset(pmc_base, 0xF7F0_0008);
        static_panic!(uart_base, "Bad SE state");
    }

    let mut buffer2 = [0u8; 0x100];
    let aligned_buffer2 = align_buffer(&mut buffer2, 0x80, 0x10);
    some_keygen(se_base, tmr_base, uart_base, aligned_buffer2);

    if aligned_buffer != aligned_buffer2 {
        static_panic!(uart_base, "keygen returned different values");
    }

    for keyslot in 0..0xa {
        aes::clear_keyslot(se_base, uart_base, keyslot);
    }

    for keyslot in 0..2 {
        rsa::clear_keyslot(se_base, uart_base, keyslot);
    }

    // I don't understand why N do this again, RNG already works, but we will do it again anywayss
    rng::setup_rng(se_base, tmr_base, uart_base);
    rng::fill_aes_register(se_base, tmr_base, uart_base, 8);
    rng::do_something_tm(se_base, tmr_base, uart_base);
}

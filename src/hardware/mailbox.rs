use tx1_a57_common::def_multi_reg32;

use super::mmu;

pub const MAILBOX_BASE: usize = 0x1f009fef8;
pub const RAW_MAILBOX_BASE: usize = 0x4000_2EF8;
def_multi_reg32!(BOOT_STATE = 0, SECMON_STATE = 4);

pub const DEBUG_MAILBOX_BASE: usize = mmu::VDBG_BASE;
def_multi_reg32!(A = 0x0, B = 0x4, C = 0x8, D = 0xC);

use core::mem::size_of;

use tx1_a57_common::{
    dsb_ish, isb,
    mmio::{self, mmio_read, mmio_write},
    read_spr,
    utils::{
        clear_and_invalidate_cache_for_address_range, cpuid, invalidate_unified_and_data_caches,
        usleep,
    },
    write_spr,
};

use super::mmu;

#[derive(Debug, Clone, Copy, Default)]
pub struct DebugRegisterState {
    pub osdtrrx_el1: u32,
    pub osdtrtx_el1: u32,
    pub mdscr_el1: u32,
    pub oseccr_el1: u32,
    pub mdccint_el1: u32,
    pub dbgclaimclr_el1: u32,
    pub dbgvcr32_el2: u32,
    pub sder32_el3: u32,
    pub mdcr_el2: u32,
    pub mdcr_el3: u32,
    pub dbgbvr0_el1: u64,
    pub dbgbcr0_el1: u64,
    pub dbgbvr1_el1: u64,
    pub dbgbcr1_el1: u64,
    pub dbgbvr2_el1: u64,
    pub dbgbcr2_el1: u64,
    pub dbgbvr3_el1: u64,
    pub dbgbcr3_el1: u64,
    pub dbgbvr4_el1: u64,
    pub dbgbcr4_el1: u64,
    pub dbgbvr5_el1: u64,
    pub dbgbcr5_el1: u64,
    pub dbgwvr0_el1: u64,
    pub dbgwcr0_el1: u64,
    pub dbgwvr1_el1: u64,
    pub dbgwcr1_el1: u64,
    pub dbgwvr2_el1: u64,
    pub dbgwcr2_el1: u64,
    pub dbgwvr3_el1: u64,
    pub dbgwcr3_el1: u64,
}

impl DebugRegisterState {
    pub fn write_to_state(&mut self) {
        self.osdtrrx_el1 = read_spr!("osdtrrx_el1") as u32;
        self.osdtrtx_el1 = read_spr!("osdtrtx_el1") as u32;
        self.mdscr_el1 = read_spr!("mdscr_el1") as u32;
        self.oseccr_el1 = read_spr!("oseccr_el1") as u32;
        self.mdccint_el1 = read_spr!("mdccint_el1") as u32;
        self.dbgclaimclr_el1 = read_spr!("dbgclaimclr_el1") as u32;
        self.dbgvcr32_el2 = read_spr!("dbgvcr32_el2") as u32;
        self.sder32_el3 = read_spr!("sder32_el3") as u32;
        self.mdcr_el2 = read_spr!("mdcr_el2") as u32;
        self.mdcr_el3 = read_spr!("mdcr_el3") as u32;
        self.dbgbvr0_el1 = read_spr!("dbgbvr0_el1");
        self.dbgbcr0_el1 = read_spr!("dbgbcr0_el1");
        self.dbgbvr1_el1 = read_spr!("dbgbvr1_el1");
        self.dbgbcr1_el1 = read_spr!("dbgbcr1_el1");
        self.dbgbvr2_el1 = read_spr!("dbgbvr2_el1");
        self.dbgbcr2_el1 = read_spr!("dbgbcr2_el1");
        self.dbgbvr3_el1 = read_spr!("dbgbvr3_el1");
        self.dbgbcr3_el1 = read_spr!("dbgbcr3_el1");
        self.dbgbvr4_el1 = read_spr!("dbgbvr4_el1");
        self.dbgbcr4_el1 = read_spr!("dbgbcr4_el1");
        self.dbgbvr5_el1 = read_spr!("dbgbvr5_el1");
        self.dbgbcr5_el1 = read_spr!("dbgbcr5_el1");
        self.dbgwvr0_el1 = read_spr!("dbgwvr0_el1");
        self.dbgwcr0_el1 = read_spr!("dbgwcr0_el1");
        self.dbgwvr1_el1 = read_spr!("dbgwvr1_el1");
        self.dbgwcr1_el1 = read_spr!("dbgwcr1_el1");
        self.dbgwvr2_el1 = read_spr!("dbgwvr2_el1");
        self.dbgwcr2_el1 = read_spr!("dbgwcr2_el1");
        self.dbgwvr3_el1 = read_spr!("dbgwvr3_el1");
        self.dbgwcr3_el1 = read_spr!("dbgwcr3_el1");
    }

    pub fn write_to_cpu(&self) {
        write_spr!("osdtrrx_el1", self.osdtrrx_el1 as u64);
        write_spr!("osdtrtx_el1", self.osdtrtx_el1 as u64);
        write_spr!("mdscr_el1", self.mdscr_el1 as u64);
        write_spr!("oseccr_el1", self.oseccr_el1 as u64);
        write_spr!("mdccint_el1", self.mdccint_el1 as u64);
        write_spr!("dbgclaimclr_el1", self.dbgclaimclr_el1 as u64);
        write_spr!("dbgvcr32_el2", self.dbgvcr32_el2 as u64);
        write_spr!("sder32_el3", self.sder32_el3 as u64);
        write_spr!("mdcr_el2", self.mdcr_el2 as u64);
        write_spr!("mdcr_el3", self.mdcr_el3 as u64);
        write_spr!("dbgbvr0_el1", self.dbgbvr0_el1);
        write_spr!("dbgbcr0_el1", self.dbgbcr0_el1);
        write_spr!("dbgbvr1_el1", self.dbgbvr1_el1);
        write_spr!("dbgbcr1_el1", self.dbgbcr1_el1);
        write_spr!("dbgbvr2_el1", self.dbgbvr2_el1);
        write_spr!("dbgbcr2_el1", self.dbgbcr2_el1);
        write_spr!("dbgbvr3_el1", self.dbgbvr3_el1);
        write_spr!("dbgbcr3_el1", self.dbgbcr3_el1);
        write_spr!("dbgbvr4_el1", self.dbgbvr4_el1);
        write_spr!("dbgbcr4_el1", self.dbgbcr4_el1);
        write_spr!("dbgbvr5_el1", self.dbgbvr5_el1);
        write_spr!("dbgbcr5_el1", self.dbgbcr5_el1);
        write_spr!("dbgwvr0_el1", self.dbgwvr0_el1);
        write_spr!("dbgwcr0_el1", self.dbgwcr0_el1);
        write_spr!("dbgwvr1_el1", self.dbgwvr1_el1);
        write_spr!("dbgwcr1_el1", self.dbgwcr1_el1);
        write_spr!("dbgwvr2_el1", self.dbgwvr2_el1);
        write_spr!("dbgwcr2_el1", self.dbgwcr2_el1);
        write_spr!("dbgwvr3_el1", self.dbgwvr3_el1);
        write_spr!("dbgwcr3_el1", self.dbgwcr3_el1);
    }

    pub const fn new() -> Self {
        Self {
            osdtrrx_el1: 0,
            osdtrtx_el1: 0,
            mdscr_el1: 0,
            oseccr_el1: 0,
            mdccint_el1: 0,
            dbgclaimclr_el1: 0,
            dbgvcr32_el2: 0,
            sder32_el3: 0,
            mdcr_el2: 0,
            mdcr_el3: 0,
            dbgbvr0_el1: 0,
            dbgbcr0_el1: 0,
            dbgbvr1_el1: 0,
            dbgbcr1_el1: 0,
            dbgbvr2_el1: 0,
            dbgbcr2_el1: 0,
            dbgbvr3_el1: 0,
            dbgbcr3_el1: 0,
            dbgbvr4_el1: 0,
            dbgbcr4_el1: 0,
            dbgbvr5_el1: 0,
            dbgbcr5_el1: 0,
            dbgwvr0_el1: 0,
            dbgwcr0_el1: 0,
            dbgwvr1_el1: 0,
            dbgwcr1_el1: 0,
            dbgwvr2_el1: 0,
            dbgwcr2_el1: 0,
            dbgwvr3_el1: 0,
            dbgwcr3_el1: 0,
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
pub struct CoreState {
    pub entrypoint: u64,
    pub ctx: u64,
}

impl CoreState {
    pub const fn new(entrypoint: u64, ctx: u64) -> Self {
        Self { entrypoint, ctx }
    }
}

pub static mut DBG_REG_STATES: [DebugRegisterState; 4] = [DebugRegisterState::new(); 4];
pub static mut CORE_STATES: [CoreState; 4] = [CoreState::new(0, 0); 4];

pub fn set_core_state(core: u8, entrypoint: u64, ctx: u64) {
    let core = core as usize & 0b11;
    unsafe {
        CORE_STATES[core].entrypoint = entrypoint;
        CORE_STATES[core].ctx = ctx;
        clear_and_invalidate_cache_for_address_range(
            CORE_STATES.as_ptr() as u64,
            size_of::<[CoreState; 4]>() as u64,
        );
    }
}

pub fn get_core_state(core: u8) -> CoreState {
    unsafe { CORE_STATES[core as usize & 0b11] }
}

pub fn write_debug_registers_from_state_to_cpu(core: u8) {
    unsafe { DBG_REG_STATES[core as usize & 0b11].write_to_cpu() }
}

pub fn write_debug_registers_from_cpu_to_state() {
    let cpuid = cpuid();
    unsafe {
        DBG_REG_STATES[cpuid as usize & 0b11].write_to_state();
    }
}

pub fn wake_up_cpu(tmr_base: usize, core: u8) {
    let device = match core {
        3 => mmio::pmc::PwrGateDevice::CE3,
        2 => mmio::pmc::PwrGateDevice::CE2,
        1 => mmio::pmc::PwrGateDevice::CE1,
        _ => mmio::pmc::PwrGateDevice::CE0,
    };

    let shifted_status = 1 << device as u32;

    if mmio_read(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_PWRGATE_STATUS) & shifted_status
        == shifted_status
    {
        return;
    }

    'outer: for _ in 0..0x1389 {
        if mmio_read(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_PWRGATE_TOGGLE) & 0x100 == 0 {
            mmio_write(
                mmu::PMC_BASE,
                mmio::pmc::APBDEV_PMC_PWRGATE_TOGGLE,
                0x100 | device as u32,
            );
            for _ in 0..0x1389 {
                if mmio_read(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_PWRGATE_STATUS) & shifted_status
                    == shifted_status
                {
                    break 'outer;
                }
                usleep(tmr_base, 1);
            }
        }
        usleep(tmr_base, 1);
    }
}

fn rw_mmio(reg: usize, val: u32) {
    mmio_write(mmu::FLOW_CONTROLLER_BASE, reg, val);
    mmio_read(mmu::FLOW_CONTROLLER_BASE, reg);
}

pub fn power_off_core() -> ! {
    let cpuid = cpuid();
    rw_mmio(
        match cpuid {
            0 => mmio::flow_controller::FLOW_CTLR_CPU0_CSR,
            1 => mmio::flow_controller::FLOW_CTLR_CPU1_CSR,
            2 => mmio::flow_controller::FLOW_CTLR_CPU2_CSR,
            _ => mmio::flow_controller::FLOW_CTLR_CPU3_CSR,
        },
        0x100 << cpuid as u32 | 0xc001,
    );

    rw_mmio(
        match cpuid {
            0 => mmio::flow_controller::FLOW_CTLR_HALT_CPU0_EVENTS,
            1 => mmio::flow_controller::FLOW_CTLR_HALT_CPU1_EVENTS,
            2 => mmio::flow_controller::FLOW_CTLR_HALT_CPU2_EVENTS,
            _ => mmio::flow_controller::FLOW_CTLR_HALT_CPU3_EVENTS,
        },
        0x40000000,
    );

    rw_mmio(
        match cpuid {
            0 => mmio::flow_controller::FLOW_CTLR_CC4_CORE0_CTRL,
            1 => mmio::flow_controller::FLOW_CTLR_CC4_CORE1_CTRL,
            2 => mmio::flow_controller::FLOW_CTLR_CC4_CORE2_CTRL,
            _ => mmio::flow_controller::FLOW_CTLR_CC4_CORE3_CTRL,
        },
        0,
    );

    write_debug_registers_from_cpu_to_state();
    invalidate_unified_and_data_caches();

    write_spr!("sctlr_el1", read_spr!("sctlr_el1") & 0xfffffffffffffffb);
    isb!();
    write_spr!("sctlr_el3", read_spr!("sctlr_el3") & 0xfffffffffffffffb);
    isb!();
    write_spr!(
        "s3_1_c15_c2_1",
        ((read_spr!("s3_1_c15_c2_1") | 0x4000000000) & 0xffffffe7ffffffff) & 0xfffffffcffffffff
    );
    isb!();
    dsb_ish!();
    write_spr!(
        "s3_1_c15_c2_1",
        (read_spr!("s3_1_c15_c2_1") | 0x4000000000) & 0xffffffffffffffbf
    );
    mmio_write(
        mmu::INTERRUPT_CONTROLLER_PCPU_BASE,
        mmio::gic400::GICC_CTLR,
        0,
    );
    write_spr!("osdlr_el1", read_spr!("osdlr_el1") | 1);
    isb!();
    dsb_ish!();
    loop {
        unsafe {
            core::arch::asm!("wfi");
        }
    }
}

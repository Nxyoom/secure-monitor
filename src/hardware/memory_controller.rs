use tx1_a57_common::mmio::{memory_controller, mmio_read, mmio_write};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct KernelCarveoutMetadata {
    address: u64,
    size: u64,
}

impl KernelCarveoutMetadata {
    pub const fn new(address: u64, size: u64) -> Self {
        Self { address, size }
    }
}

pub static mut KERNEL_CARVEOUT_METADATA: [KernelCarveoutMetadata; 2] = [
    KernelCarveoutMetadata::new(0x8006_0000, 0x1ffe_0000),
    KernelCarveoutMetadata::new(0, 0),
];

pub fn setup_carveouts(base: usize) {
    mmio_write(base, memory_controller::VIDEO_PROTECT_GPU_OVERRIDE_0, 1);
    mmio_write(base, memory_controller::VIDEO_PROTECT_GPU_OVERRIDE_1, 1);

    // TODO: Figure out what these registers are
    mmio_write(base, 0x648, 0);
    mmio_write(base, 0x64C, 0);
    mmio_write(base, 0x650, 1);

    mmio_write(base, memory_controller::SEC_CARVEOUT_BOM, 0);
    mmio_write(base, memory_controller::SEC_CARVEOUT_SIZE_MB, 0);
    mmio_write(base, memory_controller::SEC_CARVEOUT_REG_CTRL, 1);

    // TODO: Figure out what these registers are
    mmio_write(base, 0x9a0, 0);
    mmio_write(base, 0x9a4, 0);
    mmio_write(base, 0x9a8, 0);
    mmio_write(base, 0x9ac, 1);

    mmio_write(base, memory_controller::SECURITY_CFG0, 0);
    mmio_write(base, memory_controller::SECURITY_CFG1, 0);
    mmio_write(base, memory_controller::SECURITY_CFG3, 1);

    mmio_write(base, memory_controller::SECURITY_CARVEOUT_1_ADDR_LOW, 0);
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_1_ADDR_HI, 0);
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_1_SIZE_128KB, 0);
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_ACCESS_4,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_1_CLIENT_FORCE_INTERNAL_ACCESS_4,
        0,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_1_CFG, 0x4000006);

    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_ADDR_LOW,
        0x80020000,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_2_ADDR_HI, 0);
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_2_SIZE_128KB, 2);
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_ACCESS_2,
        0x3000000,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_ACCESS_4,
        0x300,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_2_CLIENT_FORCE_INTERNAL_ACCESS_4,
        0,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_2_CFG, 0x440167e);

    mmio_write(base, memory_controller::SECURITY_CARVEOUT_3_ADDR_LOW, 0);
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_3_ADDR_HI, 0);
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_3_SIZE_128KB, 0);
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_ACCESS_2,
        0x3000000,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_ACCESS_4,
        0x300,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_3_CLIENT_FORCE_INTERNAL_ACCESS_4,
        0,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_3_CFG, 0x4401e7e);

    update_carveouts_4_and_5(base);
}

pub fn update_carveouts_4_and_5(base: usize) {
    let (c4_md, c5_md) = unsafe { (KERNEL_CARVEOUT_METADATA[0], KERNEL_CARVEOUT_METADATA[1]) };

    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_ADDR_LOW,
        c4_md.address as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_ADDR_HI,
        (c4_md.address >> 32) as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_SIZE_128KB,
        (c4_md.size >> 0x11) as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_ACCESS_0,
        0x70e3407f,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_ACCESS_1,
        0x1a620880,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_ACCESS_2,
        0x303c00,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_ACCESS_3,
        0xcf0830bb,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_ACCESS_4,
        3,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_4_CLIENT_FORCE_INTERNAL_ACCESS_4,
        0,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_4_CFG, 0x8b);

    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_ADDR_LOW,
        c5_md.address as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_ADDR_HI,
        (c5_md.address >> 32) as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_SIZE_128KB,
        (c5_md.size >> 0x11) as u32,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_ACCESS_0,
        0x70e3407f,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_ACCESS_1,
        0x1a620880,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_ACCESS_2,
        0x303c00,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_ACCESS_3,
        0xcf0830bb,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_ACCESS_4,
        3,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_0,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_1,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_2,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_3,
        0,
    );
    mmio_write(
        base,
        memory_controller::SECURITY_CARVEOUT_5_CLIENT_FORCE_INTERNAL_ACCESS_4,
        0,
    );
    mmio_write(base, memory_controller::SECURITY_CARVEOUT_5_CFG, 0x8b);
}

pub fn setup_smmu(base: usize) {
    mmio_write(
        base,
        memory_controller::SMMU_TRANSLATION_ENABLE_0,
        0xffffffff,
    );
    mmio_write(
        base,
        memory_controller::SMMU_TRANSLATION_ENABLE_1,
        0xffffffff,
    );
    mmio_write(
        base,
        memory_controller::SMMU_TRANSLATION_ENABLE_2,
        0xffffffff,
    );
    mmio_write(
        base,
        memory_controller::SMMU_TRANSLATION_ENABLE_3,
        0xffffffff,
    );
    mmio_write(
        base,
        memory_controller::SMMU_TRANSLATION_ENABLE_4,
        0xffffffff,
    );

    // TODO: Figure out what these registers are
    mmio_write(base, 0x38, 0);
    mmio_write(base, 0x38, 0);
    mmio_write(base, 0x3c, 0);
    mmio_write(base, 0x9e0, 0);
    mmio_write(base, 0x9e4, 0);
    mmio_write(base, 0x9e8, 0);
    mmio_write(base, 0x9ec, 0);
    mmio_write(base, 0x9f0, 0);
    mmio_write(base, 0x9f4, 0);

    mmio_write(base, memory_controller::SMMU_PTB_DATA, 0);
    mmio_write(base, memory_controller::SMMU_TLB_CONFIG, 0x30000030);
    mmio_write(base, memory_controller::SMMU_PTC_CONFIG, 0x2800003f);
    mmio_read(base, memory_controller::SMMU_TLB_CONFIG);
    mmio_write(base, memory_controller::SMMU_PTC_FLUSH, 0);
    mmio_read(base, memory_controller::SMMU_TLB_CONFIG);
    mmio_write(base, memory_controller::SMMU_TLB_FLUSH, 0);
    mmio_read(base, memory_controller::SMMU_TLB_CONFIG);
    mmio_write(base, memory_controller::SMMU_CONFIG, 1);
    mmio_read(base, memory_controller::SMMU_TLB_CONFIG);
}

use core::arch::asm;

use tx1_a57_common::{
    mmio,
    mmu::{
        configure_registers, map_lvl1_block, map_lvl1_dtable, map_lvl2_dtable, map_lvl3_page,
        Level1Table, Level2Table, Level3Table, ATTR_INNER_SHARABLE, ATTR_MMIO_MEM, ATTR_NON_SECURE,
        ATTR_NORMAL_MEM, ATTR_NX, ATTR_RW,
    },
    utils::invalidate_unified_and_data_caches,
};

#[no_mangle]
pub static mut LVL1_TABLE: Level1Table = Level1Table([0; 0x40 / 8]);
#[no_mangle]
pub static mut LVL2_TABLE: Level2Table = Level2Table([0; 0x1000 / 8]);
#[no_mangle]
pub static mut LVL3_TABLE: Level3Table = Level3Table([0; 0x1000 / 8]);

pub const TMR_BASE: usize = 0x1f008b000;
pub const RTC_BASE: usize = 0x1f0089000;
pub const PMC_BASE: usize = RTC_BASE + 0x400;
pub const SE_BASE: usize = 0x1f008f000;
pub const CAR_BASE: usize = 0x1f0087000;
pub const VDBG_BASE: usize = 0x4002_0000;
pub const INTERRUPT_DISTRIBUTOR_BASE: usize = 0x1f0080000;
pub const INTERRUPT_CONTROLLER_PCPU_BASE: usize = 0x1f0082000;
pub const APB_BASE: usize = 0x1f0098000;
pub const SYSCTR0_BASE: usize = 0x1f0092000;
const FUSE_MAP_BASE: usize = 0x1f0096000;
pub const FUSE_BASE: usize = FUSE_MAP_BASE + 0x800;
pub const KFUSE_BASE: usize = FUSE_MAP_BASE + 0xC00;
pub const SYSTEM_REGISTERS_BASE: usize = 0x1f008d000;
pub const MEMORY_CONTROLLER_BASE: usize = 0x1f0094000;
pub const FLOW_CONTROLLER_BASE: usize = 0x1f009d000;
pub const EXCEPTION_VECTORS_BASE: usize = 0x1f00a7000;
pub const UART_BASE: usize = 0x1f0085000;
pub const PINMUX_BASE: usize = APB_BASE + 0x3000;
pub const GPIO_BASE: usize = 0x1f00a3000;

#[inline(always)]
pub fn map_lvl1_blocks() {
    let mut address = 0x80000000u64;
    for _ in 2..6 {
        unsafe {
            map_lvl1_block(&mut LVL1_TABLE, address);
        }
        address += 0x40000000;
    }
}

#[inline(always)]
pub fn unmap_lvl1_blocks() {
    unsafe {
        for entry in 2..6 {
            LVL1_TABLE.0[entry] = 0;
        }

        asm!("dsb ish", "tlbi alle3is", "dsb ish", "isb");
    }
}

#[link_section = ".text.init"]
pub fn configure_translation_table() {
    unsafe {
        map_lvl1_dtable(&mut LVL1_TABLE, &mut LVL2_TABLE, 0x40000000);
        map_lvl1_blocks();
        map_lvl1_dtable(&mut LVL1_TABLE, &mut LVL2_TABLE, 0x1c0000000);

        map_lvl2_dtable(&mut LVL2_TABLE, &mut LVL3_TABLE, 0x1f0000000);
        map_lvl2_dtable(&mut LVL2_TABLE, &mut LVL3_TABLE, 0x40000000);
        map_lvl2_dtable(&mut LVL2_TABLE, &mut LVL3_TABLE, 0x7c000000);

        map_lvl3_page(
            &mut LVL3_TABLE,
            0x7c01_0000,
            0x7c01_0000,
            0x10000,
            ATTR_INNER_SHARABLE | ATTR_RW | ATTR_NORMAL_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            0x4002_0000,
            0x4002_0000,
            0x20000,
            ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            0x1f009f000,
            0x4000_2000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            TMR_BASE as u64,
            0x6000_5000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            RTC_BASE as u64,
            0x7000_e000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            SE_BASE as u64,
            0x7001_2000,
            0x2000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            CAR_BASE as u64,
            0x6000_6000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_NON_SECURE | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            INTERRUPT_DISTRIBUTOR_BASE as u64,
            0x5004_1000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            INTERRUPT_CONTROLLER_PCPU_BASE as u64,
            0x5004_2000,
            0x2000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            APB_BASE as u64,
            0x7000_0000,
            0x4000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            SYSCTR0_BASE as u64,
            0x700f_0000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            FUSE_MAP_BASE as u64,
            0x7000_f000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            SYSTEM_REGISTERS_BASE as u64,
            mmio::system_registers::SYSTEM_REGISTERS_BASE as u64,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            MEMORY_CONTROLLER_BASE as u64,
            0x7001_9000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            FLOW_CONTROLLER_BASE as u64,
            0x6000_7000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            EXCEPTION_VECTORS_BASE as u64,
            0x6000_f000,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            UART_BASE as u64,
            mmio::uart::UART_BASE as u64,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );
        map_lvl3_page(
            &mut LVL3_TABLE,
            GPIO_BASE as u64,
            mmio::gpio::GPIO_BASE as u64,
            0x1000,
            ATTR_NX | ATTR_INNER_SHARABLE | ATTR_RW | ATTR_MMIO_MEM,
        );

        configure_registers(&LVL1_TABLE);
        invalidate_unified_and_data_caches();
    }
}

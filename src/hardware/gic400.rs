//! N's code is a *bit* incorrect. Just a *bit*.
//!
//! Implemented based on [Generic Interrupt Controller Spec](https://developer.arm.com/documentation/ihi0048/b/Introduction/About-the-Generic-Interrupt-Controller-architecture?lang=en) and the [GIC-400 Manual](https://developer.arm.com/documentation/ddi0471/a) rather than reverse engineering of the secure monitor

use tx1_a57_common::mmio::{gic400, mmio_read, mmio_write};

const IPRIORITY_OFFSET: usize = gic400::GICD_IPRIORITY_R00;
const ISENABLE_OFFSET: usize = gic400::GICD_ISENABLER_R0;
const ICFG_OFFSET: usize = gic400::GICD_ICFG_R00;
const ITARGET_OFFSET: usize = gic400::GICD_ITARGETS_R00;
const IGROUP_OFFSET: usize = gic400::GICD_IGROUP_R0;

pub fn setup(gicd_base: usize) {
    mmio_write(gicd_base, gic400::GICD_IGROUP_R1, 0xFFFF_FFFF);
    mmio_write(gicd_base, gic400::GICD_IGROUP_R2, 0xFFFF_FFFF);
    mmio_write(gicd_base, gic400::GICD_IGROUP_R3, 0xFFFF_FFFF);
    mmio_write(gicd_base, gic400::GICD_IGROUP_R4, 0xFFFF_FFFF);
    mmio_write(gicd_base, gic400::GICD_IGROUP_R5, 0xFFFF_FFFF);

    // N's implementation violates spec
    for x in 0x20..0xe0 {
        gicd_set_prio(gicd_base, x, 0x80);
    }

    mmio_write(gicd_base, gic400::GICD_CTLR, 1);
}

pub fn setup_core(gicd_base: usize, gicc_base: usize) {
    mmio_write(gicd_base, gic400::GICD_IGROUP_R0, 0xFFFF_FFFF);

    // N's implementation violates spec
    for x in 0x00..0x20 {
        gicd_set_prio(gicd_base, x, 0x80);
    }

    mmio_write(gicc_base, gic400::GICC_CTLR, 0x1D9);
    mmio_write(gicc_base, gic400::GICC_PMR, 0x80);
    mmio_write(gicc_base, gic400::GICC_BPR, 0x7);
}

pub fn set_gicc_end_of_interrupt_register(gicc_base: usize, value: u32) {
    mmio_write(gicc_base, gic400::GICC_EOIR, value);
}

pub fn get_gicc_interrupt_ack_reg(gicc_base: usize) -> u32 {
    mmio_read(gicc_base, gic400::GICC_IAR)
}

pub fn gicd_set_irq_enb(gicd_base: usize, irq: u32) {
    let reg_set = irq as usize / 0x20;
    let reg = ISENABLE_OFFSET + (reg_set * 4);
    mmio_write(gicd_base, reg, 1 << (irq % 0x20));
}

pub fn gicd_set_cfg(gicd_base: usize, irq: u32, cfg: gic400::GicdCfg) {
    let reg = ICFG_OFFSET + ((irq as usize / 0x10) * 4);
    let shift = (irq % 0x10) * 2;
    let orig_val = mmio_read(gicd_base, reg);

    mmio_write(
        gicd_base,
        reg,
        (orig_val & !(0b11 << shift)) | ((cfg as u32) << shift),
    );
}

// Limited in the fact that we can only assign one CPU to each interrupt. Maybe we will have to change this but I doubt it.
pub fn gicd_set_target_cpu(gicd_base: usize, irq: u32, cpu_id: u8) {
    // This looks slightly confusing, but each ITARGET register contains 4 irqs, and is 32bit wide
    let reg = ITARGET_OFFSET + ((irq as usize / 4) * 4);
    let shift = (irq % 4) * 8;
    let orig_val = mmio_read(gicd_base, reg);

    // It is a bitfield where each bit is a CPUID, supporting up to 8 CPUs
    mmio_write(
        gicd_base,
        reg,
        (orig_val & !(0b1111_1111 << shift)) | ((1 << cpu_id) << shift),
    );
}

pub fn gicd_set_irq_group(gicd_base: usize, irq: u32, group_1: bool) {
    let reg = IGROUP_OFFSET + ((irq as usize / 0x20) * 4);
    let shift = irq % 0x20;
    let orig_val = mmio_read(gicd_base, reg);

    mmio_write(
        gicd_base,
        reg,
        match group_1 {
            true => orig_val | 1 << shift,
            false => orig_val & !(1 << shift),
        },
    );
}

pub fn gicd_set_prio(gicd_base: usize, irq: u32, prio: u8) {
    // This looks slightly confusing, but each IPRIORITY register contains 4 irqs, and is 32bit wide
    let reg = IPRIORITY_OFFSET + ((irq as usize / 4) * 4);
    let shift = irq % 4;
    let orig_val = mmio_read(gicd_base, reg);

    mmio_write(
        gicd_base,
        reg,
        (orig_val & !(0b1111_1111 << shift)) | ((prio as u32) << shift),
    );
}

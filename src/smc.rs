use spin::Mutex;
use tx1_a57_common::{
    hardware::{tmr::full_system_reset_via_watchdog, HardwareState, HardwareType},
    log,
    mmio::{self, mmio_read, mmio_write},
    utils::{colour_to_scratch_contents, cpuid, set_pmc_scratch_200_if_unset},
};

use crate::hardware::{get_device_id, mailbox, mmu};

pub static USERMODE_SMC_RUNNING: Mutex<()> = Mutex::new(());

pub mod smc0;
pub mod smc1;

#[repr(C)]
pub struct CallingRegisters {
    pub x0: u64,
    pub x1: u64,
    pub x2: u64,
    pub x3: u64,
    pub x4: u64,
    pub x5: u64,
    pub x6: u64,
    pub x7: u64,
    pub x8: u64,
    pub x9: u64,
    pub x10: u64,
    pub x11: u64,
    pub x12: u64,
    pub x13: u64,
    pub x14: u64,
    pub x15: u64,
    pub x16: u64,
    pub x17: u64,
    pub x18: u64,
    pub x19: u64,
    pub x29: u64,
    pub x30: u64,
}

pub fn handle_smc(smc_imm16: u16, sub_id: u8, args: &mut CallingRegisters) {
    mmio_write(mailbox::DEBUG_MAILBOX_BASE, mailbox::B, cpuid() as u32);
    mmio_write(mailbox::DEBUG_MAILBOX_BASE, mailbox::D, sub_id as u32);

    if smc_imm16 == 0 && cpuid() == 3 {
        match sub_id {
            2 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC0 ID 2 called\n");
                smc_generic_get_config(args);
            }
            6 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC0 ID 6 called\n");
                let guard = USERMODE_SMC_RUNNING.lock();
                smc0::generate_random_bytes::smc0_generate_random_bytes(args);
                drop(guard);
            }
            7 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC0 ID 7 called\n");
                let guard = USERMODE_SMC_RUNNING.lock();
                smc0::generate_aes_kek::smc0_generate_aes_kek(args);
                drop(guard);
            }
            _ => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC0 ID unimplemented called\n");
                set_pmc_scratch_200_if_unset(
                    mmu::PMC_BASE,
                    colour_to_scratch_contents(0, 0xFF, 0xFF, sub_id as u32),
                );
                full_system_reset_via_watchdog(mmu::TMR_BASE);
            }
        }
    } else if smc_imm16 == 1 {
        match sub_id {
            //2 => smc1::power_off_cpu::smc1_power_off_cpu(args),
            3 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 3 called\n");
                smc1::start_cpu::smc1_start_cpu(args);
            }
            4 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 4 called\n");
                smc_generic_get_config(args);
            }
            5 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 5 called\n");
                smc1::generate_random_bytes::smc1_generate_random_bytes(args);
            }
            6 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 6 called\n");
                smc1::panic::smc1_panic(args);
            }
            7 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 7 called\n");
                smc1::set_kernel_carveout::smc1_set_kernel_carveout(args);
            }
            8 => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID 8 called\n");
                smc1::read_write_register::smc1_read_write_register(args);
            }
            _ => {
                log::write(mmu::UART_BASE, "[Nxyoom] SMC1 ID unimplemented called\n");
                set_pmc_scratch_200_if_unset(
                    mmu::PMC_BASE,
                    colour_to_scratch_contents(0, 0xFF, 0xFF, sub_id as u32),
                );
                full_system_reset_via_watchdog(mmu::TMR_BASE);
            }
        }
    }
}

/// ## Safety
///
/// Don't call with a bad pointer
#[no_mangle]
pub unsafe extern "C" fn decode_smc(smc_imm16: u16, args: *mut CallingRegisters) {
    let sub_id = (*args).x0;
    let masked_sub_id = (sub_id & 0xFF) as u8;

    handle_smc(
        smc_imm16,
        masked_sub_id,
        args.as_mut().expect("NullPtr args in decode_smc"),
    );
}

fn smc_generic_get_config(args: &mut CallingRegisters) {
    let config_option = args.x1 as u32 - 1;

    args.x0 = 0;
    match config_option {
        // Disable Program Verification
        0 => {
            args.x1 = 1;
        }
        // DRAM ID
        1 => {
            args.x1 =
                ((mmio_read(mmu::FUSE_BASE, mmio::fuse::CACHE_FUSE_RESERVED_ODM4) >> 3) & 7) as u64;
        }
        // SE Interrupt Number
        2 => args.x1 = 0x2c,
        // Fuse version
        3 => {
            args.x1 = 0x4;
        }
        // Hardware Type
        4 => {
            args.x1 = HardwareType::read_from_fuse(mmu::FUSE_BASE) as u64;
        }
        // Hardware State
        5 => {
            args.x1 = HardwareState::read_from_fuse(mmu::FUSE_BASE) as u64;
        }
        // Is Recovery Boot
        6 => {
            args.x1 = false as u64;
        }
        // Device ID
        7 => {
            args.x1 = get_device_id();
        }
        // Boot reason
        8 => {
            args.x1 = 2;
        }
        // Memory Mode
        9 => {
            args.x1 = 1;
        }
        // Debug mode
        10 => {
            args.x1 = 0;
        }
        // Kernel config
        11 => {
            // config |= 1 << 8; // Show error on panic
            // config |= 3 << 16; // 4GB memory
            args.x1 = 0;
        }
        // Is charger active
        12 => {
            args.x1 = 0;
        }
        _ => {
            args.x0 = 2;
        }
    }
}

use core::{arch::asm, panic::PanicInfo};

use tx1_a57_common::{
    hardware::security_engine::panic_if_se_isnt_idle,
    isb, log,
    mmio::{self, ahb, apb, mmio_and, mmio_read, mmio_write, mselect, syscrt0},
    mmu::configure_registers,
    read_spr, static_panic,
    utils::{cpuid, invalidate_unified_and_data_caches},
    write_spr,
};

use crate::{
    hardware::{
        self,
        mmu::{self, configure_translation_table},
    },
    vectors::setup_vector_table,
};

/// This is useless because of `panic_immediate_abort`
#[panic_handler]
fn panic(_panic_info: &PanicInfo<'_>) -> ! {
    loop {}
}

#[link_section = ".text.start"]
#[naked]
#[no_mangle]
pub extern "C" fn _start() -> ! {
    extern "C" {
        static __stack_top: u8;
    }

    unsafe {
        asm!(
            "MSR daifset, #0xf",
            // Begin branch target buffer invalidation
            // s3_1_c15_c2_0 is CPUACTLR_EL1
            "MRS X0, s3_1_c15_c2_0",
            "ORR X0, X0, #0x1",
            "MSR s3_1_c15_c2_0, X0", //  Enable invalidation of BTB on IC IALLU
            "DSB SY",
            "ISB",
            "IC IALLU", // Instruction cache invalidation; Clears branch target buffer too currently
            "DSB SY",
            "ISB",
            "MRS X0, s3_1_c15_c2_0",
            "AND X0, X0, #0xfffffffffffffffe",
            "MSR s3_1_c15_c2_0, X0", //  Enable invalidation of BTB on IC IALLU
            "DSB SY",
            "ISB",
            // End branch target buffer invalidation

            // Check OS Lock status, if so clear it and request a warm reset
            "MRS X0, OSLSR_EL1",
            "ANDS X0, X0, #0x2",
            "B.EQ 1f",
            "MOV X0, XZR",
            "MSR OSLAR_EL1, X0",
            "MOV X0, #0x8000000000000000",
            "MSR s3_1_c15_c2_0, X0", // Forces the processor RCG to high (Register is CPUACTLR_EL1)
            "ISB",
            "MOV X0, #0x3",
            "MSR RMR_EL3, X0", // Set reset state to A64 and request a warm reset
            "ISB",
            "DSB SY",
            "WFI",
            ".rept 0x40",
            "NOP", // 0x40 NOPs for some reason
            ".endr",
            "1:",
            "MOV X0, #0x1",
            "MSR OSLAR_EL1, X0",     // Enable OS Lock
            "MSR SPSEL, #0",         // Use SP_EL0 at all Exception levels
            "ADRP X20, __stack_top", // Place stack pointer 0x1000 bytes into TZRAM
            "MOV SP, X20",
            "B  _init",
            options(noreturn),
        )
    }
}

#[link_section = ".text.init"]
pub fn clear_bss() {
    extern "C" {
        static __bss_start: u8;
        static __bss_end: u8;
    }

    unsafe {
        let start = (&__bss_start) as *const _ as usize;
        let end = (&__bss_end) as *const _ as usize;

        for ptr in start..end {
            *(ptr as *mut u8) = 0;
        }
    }
}

#[link_section = ".text.init"]
pub fn relocate() {
    extern "C" {
        static __text_start_end: u8;
        static __end: u8;
        static __text_start: u8;
    }

    unsafe {
        let unrelocated_start = (&__text_start_end) as *const _ as usize;
        let relocated_end = (&__end) as *const _ as usize;
        let relocated_start = (&__text_start) as *const _ as usize;
        let length = relocated_end - relocated_start;

        if length % 8 != 0 {
            static_panic!(
                mmu::UART_BASE,
                "[Nxyoom] Payload size not aligned to 8 bytes"
            );
        }

        for x in 0..(length / 8) {
            *((relocated_start + (x * 8)) as *mut u64) =
                *((unrelocated_start + (x * 8)) as *mut u64)
        }
    }

    invalidate_unified_and_data_caches();
}

#[link_section = ".text.init"]
fn setup_registers() {
    invalidate_unified_and_data_caches();

    mmio_write(
        syscrt0::SYSCRT0_BASE,
        syscrt0::SYSCRT0_CNTCR,
        syscrt0::CNTCR_ENABLE | syscrt0::CNTCR_HALT_ON_DEBUG,
    );
    // Unk
    mmio_and(0, 0x500038f8, 0xfffffffe);
    // Unk
    mmio_write(0, 0x50003300, 0);
    mmio_write(ahb::AHB_BASE, ahb::AHB_MASTER_SWID_0, 0);
    mmio_write(ahb::AHB_BASE, ahb::AHB_MASTER_SWID_1, 0);
    mmio_write(apb::APB_BASE, apb::APBDMA_CHANNEL_SWID, 0xffffffff);
    mmio_write(apb::APB_BASE, apb::APBDMA_CHANNEL_SWID1, 0);
    mmio_write(apb::APB_BASE, apb::APBDMA_SECURITY_REG, 0);
    mmio_and(mselect::MSELECT_BASE, mselect::MSELECT_CONFIG, 0xc4ffffff);
    mmio_write(
        ahb::AHB_BASE,
        ahb::AHB_ARBITRATION_DISABLE,
        ahb::AHB_DISABLE_USB_ARB | ahb::AHB_DISABLE_USB2_ARB,
    );
    mmio_write(
        ahb::AHB_BASE,
        ahb::AHB_ARBITRATION_PRIORITY_CTRL,
        (0b111 << 29) | 1,
    ); // Set arbitration weight to the highest
    mmio_write(
        ahb::AHB_BASE,
        ahb::AHB_GIZMO_TZRAM,
        ahb::AHB_GIZMO_TZRAM_DONT_SPLIT_WRITE,
    );
}

#[no_mangle]
#[link_section = ".text.init"]
pub extern "C" fn _init() -> ! {
    clear_bss();

    setup_registers();

    relocate();

    configure_translation_table();

    continued_init();
}

// This init function is in TZRAM
fn continued_init() -> ! {
    log::init(
        mmu::GPIO_BASE,
        mmu::PINMUX_BASE,
        mmu::CAR_BASE,
        mmu::UART_BASE,
        mmu::TMR_BASE,
    );

    setup_vector_table();

    crate::main();
}

#[naked]
pub extern "C" fn read_caller() -> u64 {
    unsafe {
        asm!("MOV X0, X30", "RET", options(noreturn));
    }
}

pub fn route_external_aborts_and_serrors_to_el3(val: bool) {
    let orig_val = read_spr!("SCR_EL3");

    if val {
        write_spr!("SCR_EL3", orig_val | (1 << 3));
    } else {
        write_spr!("SCR_EL3", orig_val & !(1 << 3));
    }
}

pub fn per_core_setup() {
    let value = mmio_read(mmu::SYSCTR0_BASE, mmio::syscrt0::SYSCTR0_CNTFID0);
    write_spr!("actlr_el3", 0x73u64);
    write_spr!("actlr_el2", 0x73u64);
    write_spr!("hcr_el2", 0x80000000u64);
    write_spr!("dacr32_el2", 0xffffffffu64);
    write_spr!("sctlr_el1", 0xc50838u64);
    write_spr!("sctlr_el2", 0x30c50838u64);
    isb!();
    write_spr!("cntfrq_el0", value as u64);
    write_spr!("cnthctl_el2", 3u64);
    isb!();

    let cpuid = cpuid();

    mmio_write(
        mmu::FLOW_CONTROLLER_BASE,
        match cpuid {
            0 => mmio::flow_controller::FLOW_CTLR_CPU0_CSR,
            1 => mmio::flow_controller::FLOW_CTLR_CPU1_CSR,
            2 => mmio::flow_controller::FLOW_CTLR_CPU2_CSR,
            _ => mmio::flow_controller::FLOW_CTLR_CPU3_CSR,
        },
        0,
    );

    mmio_write(
        mmu::FLOW_CONTROLLER_BASE,
        match cpuid {
            0 => mmio::flow_controller::FLOW_CTLR_HALT_CPU0_EVENTS,
            1 => mmio::flow_controller::FLOW_CTLR_HALT_CPU1_EVENTS,
            2 => mmio::flow_controller::FLOW_CTLR_HALT_CPU2_EVENTS,
            _ => mmio::flow_controller::FLOW_CTLR_HALT_CPU3_EVENTS,
        },
        0,
    );

    hardware::gic400::setup_core(
        mmu::INTERRUPT_DISTRIBUTOR_BASE,
        mmu::INTERRUPT_CONTROLLER_PCPU_BASE,
    );
    hardware::gic400::gicd_set_prio(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x1c, 0);
    hardware::gic400::gicd_set_irq_group(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x1c, false);
    hardware::gic400::gicd_set_irq_enb(mmu::INTERRUPT_DISTRIBUTOR_BASE, 0x1c);
}

#[link_section = ".text.reset"]
#[naked]
#[no_mangle]
pub extern "C" fn reset_vector() {
    extern "C" {
        static __stack_top: u8;
    }

    unsafe {
        asm!(
            "MSR daifset, #0xf",
            // Begin branch target buffer invalidation
            // s3_1_c15_c2_0 is CPUACTLR_EL1
            "MRS X0, s3_1_c15_c2_0",
            "ORR X0, X0, #0x1",
            "MSR s3_1_c15_c2_0, X0", //  Enable invalidation of BTB on IC IALLU
            "DSB SY",
            "ISB",
            "IC IALLU", // Instruction cache invalidation; Clears branch target buffer too currently
            "DSB SY",
            "ISB",
            "MRS X0, s3_1_c15_c2_0",
            "AND X0, X0, #0xfffffffffffffffe",
            "MSR s3_1_c15_c2_0, X0", //  Enable invalidation of BTB on IC IALLU
            "DSB SY",
            "ISB",
            // End branch target buffer invalidation

            // Check OS Lock status, if so clear it and request a warm reset
            "MRS X0, OSLSR_EL1",
            "ANDS X0, X0, #0x2",
            "B.EQ 1f",
            "MOV X0, XZR",
            "MSR OSLAR_EL1, X0",
            "MOV X0, #0x8000000000000000",
            "MSR s3_1_c15_c2_0, X0", // Forces the processor RCG to high (Register is CPUACTLR_EL1)
            "ISB",
            "MOV X0, #0x3",
            "MSR RMR_EL3, X0", // Set reset state to A64 and request a warm reset
            "ISB",
            "DSB SY",
            "WFI",
            ".rept 0x40",
            "NOP", // 0x40 NOPs for some reason
            ".endr",
            "1:",
            "MOV X0, #0x1",
            "MSR OSLAR_EL1, X0", // Enable OS Lock
            "MSR SPSEL, #1",     // Use SP_ELx
            "ADRP X20, __stack_top",
            "MRS X21, MPIDR_EL1",
            "AND X21, X21, #0x3",
            "MOV X22, #0x400",
            "MSUB X20, X21, X22, X20",
            "MOV SP, X20",
            "B  reset_vector_init",
            options(noreturn),
        )
    }
}

#[no_mangle]
pub extern "C" fn reset_vector_init() {
    if mmio_read(
        mmio::memory_controller::MC_BASE,
        mmio::memory_controller::SECURITY_CFG3,
    ) == 0
    {
        invalidate_unified_and_data_caches();

        mmio_write(
            syscrt0::SYSCRT0_BASE,
            syscrt0::SYSCRT0_CNTCR,
            syscrt0::CNTCR_ENABLE | syscrt0::CNTCR_HALT_ON_DEBUG,
        );
        // Unk
        mmio_and(0, 0x500038f8, 0xfffffffe);
        // Unk
        mmio_write(0, 0x50003300, 0);
        mmio_write(ahb::AHB_BASE, ahb::AHB_MASTER_SWID_0, 0);
        mmio_write(ahb::AHB_BASE, ahb::AHB_MASTER_SWID_1, 0);
        mmio_write(apb::APB_BASE, apb::APBDMA_CHANNEL_SWID, 0xffffffff);
        mmio_write(apb::APB_BASE, apb::APBDMA_CHANNEL_SWID1, 0);
        mmio_write(apb::APB_BASE, apb::APBDMA_SECURITY_REG, 0);
        mmio_write(
            mselect::MSELECT_BASE,
            mselect::MSELECT_CONFIG,
            (mmio_read(mselect::MSELECT_BASE, mselect::MSELECT_CONFIG) & 0xc4ffffff) | 0x38000000,
        );
        mmio_write(
            ahb::AHB_BASE,
            ahb::AHB_ARBITRATION_DISABLE,
            ahb::AHB_DISABLE_USB_ARB | ahb::AHB_DISABLE_USB2_ARB,
        );
        mmio_write(
            ahb::AHB_BASE,
            ahb::AHB_ARBITRATION_PRIORITY_CTRL,
            (0b111 << 29) | 1,
        ); // Set arbitration weight to the highest
        mmio_write(
            ahb::AHB_BASE,
            ahb::AHB_GIZMO_TZRAM,
            ahb::AHB_GIZMO_TZRAM_DONT_SPLIT_WRITE,
        );
    }

    unsafe {
        configure_registers(&mmu::LVL1_TABLE);
    }
    invalidate_unified_and_data_caches();

    setup_vector_table();

    if mmio_read(
        mmu::MEMORY_CONTROLLER_BASE,
        mmio::memory_controller::SECURITY_CFG3,
    ) == 0
    {
        panic_if_se_isnt_idle(mmu::SE_BASE, mmu::UART_BASE);
        hardware::gic400::setup(mmu::INTERRUPT_DISTRIBUTOR_BASE);
        static_panic!(mmu::UART_BASE, "TODO");
    }

    per_core_setup();

    route_external_aborts_and_serrors_to_el3(false);

    drop_to_el1();
}

pub fn drop_to_el1() -> ! {
    extern "C" {
        static __stack_top: u8;
    }

    let core_state = hardware::cpu::get_core_state(cpuid());

    let sp = unsafe { &__stack_top as *const _ as usize } - (cpuid() as usize * 0x400);
    let ep = core_state.entrypoint;
    let ctx = core_state.ctx;

    unsafe {
        asm!(
            "MSR SPSel, 1",
            "MOV SP, {1}",
            "MSR ELR_EL3, {0}",
            "DSB SY",
            "SEV",
            "MOV X0, #0x3c5",
            "MSR SPSR_EL3, X0",
            "MOV X0, {2}",
            "ISB",
            "ERET",
            in(reg) ep,
            in(reg) sp,
            in(reg) ctx,
            options(noreturn)
        );
    }
}

pub fn setup_reset_vector() {
    // TODO: Figure out what this register is
    mmio_write(mmu::EXCEPTION_VECTORS_BASE, 0x100, 0);

    let reset_vec = reset_vector as usize as u64;
    mmio_write(
        mmu::SYSTEM_REGISTERS_BASE,
        mmio::system_registers::SB_AA64_RESET_LOW,
        reset_vec as u32 | 1,
    );
    mmio_write(
        mmu::SYSTEM_REGISTERS_BASE,
        mmio::system_registers::SB_AA64_RESET_HIGH,
        (reset_vec >> 32) as u32,
    );
    mmio_write(
        mmu::SYSTEM_REGISTERS_BASE,
        mmio::system_registers::SB_CSR,
        2,
    );
    mmio_read(mmu::SYSTEM_REGISTERS_BASE, mmio::system_registers::SB_CSR);
    mmio_write(
        mmu::PMC_BASE,
        mmio::pmc::APBDEV_PMC_SECURE_SCRATCH34,
        reset_vec as u32,
    );
    mmio_write(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_SECURE_SCRATCH35, 0);
    mmio_write(mmu::PMC_BASE, mmio::pmc::APBDEV_PMC_SEC_DISABLE3, 0x500000);
}

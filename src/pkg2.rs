use tx1_a57_common::{
    dsb_ish,
    hardware::security_engine,
    mmio::mmio_write,
    static_panic,
    utils::{
        align_buffer, clear_and_invalidate_cache_for_address_range,
        invalidate_unified_and_data_caches, set_pmc_scratch_200_if_unset,
    },
};

use crate::hardware::{
    mailbox::{self, DEBUG_MAILBOX_BASE},
    mmu,
};

pub const PKG2_OUTPUT_BASE: u64 = 0x80000000;

pub const PKG2_ADDR: u64 = 0xA980_0000;
pub const WRAPPED_KEY: &[u8; 0x10] =
    b"\xFB\x8B\x6A\x9C\x79\x00\xC8\x49\xEF\xD2\x4D\x85\x4D\x30\xA0\xC7";

fn do_pkg2_dec(output: &mut [u8], input: &[u8], ctr: &[u8]) {
    security_engine::aes::aes_ctr_decrypt_with_wrapped_key(
        mmu::SE_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
        output,
        input,
        WRAPPED_KEY,
        ctr,
    );
}
#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct RawPackage2Header {
    pub signature: [u8; 0x100],
    pub encrypted_header: [u8; 0x100],
}

#[repr(C)]
pub struct Package2Header {
    pub header_ctr: [u8; 0x10],
    pub section_ctrs: [[u8; 0x10]; 4],
    pub magic: u32, // PK21
    pub base_offset: u32,
    res: u32,
    pub pkg2_version: u8,
    pub bootloader_version: u8,
    pad: u16,
    pub section_sizes: [u32; 4],
    pub section_offsets: [u32; 4],
    pub sha256_over_encrypted_sections: [[u8; 0x20]; 4],
}

#[derive(Debug, Clone, Copy)]
pub enum Package2ValidationError {
    InvalidMagic,
    BadKeyGeneration,
    UnalignedBase,
    UnalignedSection(u8),
    InvalidSizeFromCtr,
    Section0OverlapsSection1,
    Section1OverlapsSection2,
    Section0OverlapsSection2,
    Section0LargerThanBaseOffset,
    Section1LargerThanBaseOffset,
    Section2LargerThanBaseOffset,
    InvalidBootloaderVersion,
    InvalidPkg2Version,
}

impl Package2ValidationError {
    pub fn panic(self) {
        set_pmc_scratch_200_if_unset(mmu::PMC_BASE, 0xF0000002);
        match self {
            Package2ValidationError::InvalidMagic => static_panic!(mmu::UART_BASE, "Package2 Magic is invalid"),
            Package2ValidationError::BadKeyGeneration => static_panic!(mmu::UART_BASE, "Bad key generation"),
            Package2ValidationError::UnalignedBase => static_panic!(mmu::UART_BASE, "Unaligned base offset"),
            Package2ValidationError::UnalignedSection(sec) => match sec {
                0 => static_panic!(mmu::UART_BASE, "Section 0 offset is unaligned"),
                1 => static_panic!(mmu::UART_BASE, "Section 1 offset is unaligned"),
                2 => static_panic!(mmu::UART_BASE, "Section 2 offset is unaligned"),
                3 => static_panic!(mmu::UART_BASE, "Section 3 offset is unaligned"),
                _ => static_panic!(mmu::UART_BASE, "Invalid Section(Should be unreachable, something very wrong happened) offset is unaligned"),
            },
            Package2ValidationError::InvalidSizeFromCtr => static_panic!(mmu::UART_BASE, "CTR size is incorrect"),
            Package2ValidationError::Section0OverlapsSection1 => static_panic!(mmu::UART_BASE, "Section0 Overlaps Section1"),
            Package2ValidationError::Section1OverlapsSection2 => static_panic!(mmu::UART_BASE, "Section1 Overlaps Section2"),
            Package2ValidationError::Section0OverlapsSection2 => static_panic!(mmu::UART_BASE, "Section0 Overlaps Section2"),
            Package2ValidationError::Section0LargerThanBaseOffset => static_panic!(mmu::UART_BASE, "Section0 is larger than the Base Offset"),
            Package2ValidationError::Section1LargerThanBaseOffset => static_panic!(mmu::UART_BASE, "Section1 is larger than the Base Offset"),
            Package2ValidationError::Section2LargerThanBaseOffset => static_panic!(mmu::UART_BASE, "Section2 is larger than the Base Offset"),
            Package2ValidationError::InvalidBootloaderVersion => static_panic!(mmu::UART_BASE, "Invalid bootloader version"),
            Package2ValidationError::InvalidPkg2Version => static_panic!(mmu::UART_BASE, "Invalid PKG2 version"),
        }
    }
}

impl Package2Header {
    pub fn validate(&self) -> Result<(), Package2ValidationError> {
        let parsed_ctr = ParsedCtr::new(&self.header_ctr);

        if self.magic != u32::from_le_bytes(*b"PK21") {
            return Err(Package2ValidationError::InvalidMagic);
        }

        // if parsed_ctr.key_generation != 3 {
        // return Err(Package2ValidationError::BadKeyGeneration);
        // }

        let base_offset = self.base_offset as u64;

        if base_offset & 3 != 0 {
            return Err(Package2ValidationError::UnalignedBase);
        }

        for (i, &section_offset) in self.section_offsets.iter().enumerate() {
            if section_offset & 3 != 0 {
                return Err(Package2ValidationError::UnalignedSection(i as u8));
            }
        }

        let section0_size = self.section_sizes[0] as u64;
        let section1_size = self.section_sizes[1] as u64;
        let section2_size = self.section_sizes[2] as u64;

        if section0_size + section1_size + section2_size + 0x200 != parsed_ctr.pkg2_raw_size as u64
        {
            return Err(Package2ValidationError::InvalidSizeFromCtr);
        }

        let section0_offset = self.section_offsets[0] as u64;
        let section1_offset = self.section_offsets[1] as u64;
        let section2_offset = self.section_offsets[2] as u64;

        let section0_end = section0_offset + section0_size;
        let section1_end = section1_offset + section1_size;
        let section2_end = section2_offset + section2_size;

        if section0_end > section1_offset && section0_offset < section1_offset {
            return Err(Package2ValidationError::Section0OverlapsSection1);
        }

        if section1_end > section2_offset && section1_offset < section2_offset {
            return Err(Package2ValidationError::Section1OverlapsSection2);
        }

        if section0_end > section2_offset && section0_offset < section2_offset {
            return Err(Package2ValidationError::Section0OverlapsSection2);
        }

        if section0_end > base_offset && section0_offset < base_offset {
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::C, section0_end as u32);
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::D, base_offset as u32);
            return Err(Package2ValidationError::Section0LargerThanBaseOffset);
        }

        if section1_end > base_offset && section1_offset < base_offset {
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::C, section1_end as u32);
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::D, base_offset as u32);
            return Err(Package2ValidationError::Section1LargerThanBaseOffset);
        }

        if section2_end > base_offset && section2_offset < base_offset {
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::C, section2_end as u32);
            mmio_write(DEBUG_MAILBOX_BASE, mailbox::D, base_offset as u32);
            return Err(Package2ValidationError::Section2LargerThanBaseOffset);
        }

        if self.bootloader_version > 4 {
            return Err(Package2ValidationError::InvalidBootloaderVersion);
        }

        if self.pkg2_version < 6 {
            return Err(Package2ValidationError::InvalidPkg2Version);
        }

        Ok(())
    }

    pub fn unpack(&self) -> UnpackedPackage2 {
        let section0_output_addr = (self.section_offsets[0] as u64 + PKG2_OUTPUT_BASE) as *mut u8;
        let section1_output_addr = (self.section_offsets[1] as u64 + PKG2_OUTPUT_BASE) as *mut u8;
        let section2_output_addr = (self.section_offsets[2] as u64 + PKG2_OUTPUT_BASE) as *mut u8;

        let section0_enc_addr = PKG2_ADDR + 0x200;
        let section1_enc_addr = section0_enc_addr + self.section_sizes[0] as u64;
        let section2_enc_addr = section1_enc_addr + self.section_offsets[1] as u64;

        let section0_input_slice = unsafe {
            &*core::ptr::slice_from_raw_parts(
                section0_enc_addr as *mut u8,
                self.section_sizes[0] as usize,
            )
        };
        let section0_output_slice = unsafe {
            &mut *core::ptr::slice_from_raw_parts_mut(
                section0_output_addr,
                self.section_sizes[0] as usize,
            )
        };

        let section1_input_slice = unsafe {
            &*core::ptr::slice_from_raw_parts(
                section1_enc_addr as *mut u8,
                self.section_sizes[1] as usize,
            )
        };
        let section1_output_slice = unsafe {
            &mut *core::ptr::slice_from_raw_parts_mut(
                section1_output_addr,
                self.section_sizes[1] as usize,
            )
        };

        let section2_input_slice = unsafe {
            &*core::ptr::slice_from_raw_parts(
                section2_enc_addr as *mut u8,
                self.section_sizes[2] as usize,
            )
        };
        let section2_output_slice = unsafe {
            &mut *core::ptr::slice_from_raw_parts_mut(
                section2_output_addr,
                self.section_sizes[2] as usize,
            )
        };

        do_pkg2_dec(
            section0_output_slice,
            section0_input_slice,
            &self.section_ctrs[0],
        );
        do_pkg2_dec(
            section1_output_slice,
            section1_input_slice,
            &self.section_ctrs[1],
        );
        do_pkg2_dec(
            section2_output_slice,
            section2_input_slice,
            &self.section_ctrs[2],
        );

        invalidate_unified_and_data_caches();

        UnpackedPackage2::new(
            section0_output_addr as u64,
            section1_output_addr as u64,
            section2_output_addr as u64,
        )
    }
}

#[derive(Debug, Clone, Copy)]
pub struct UnpackedPackage2 {
    pub section0_addr: u64,
    pub section1_addr: u64,
    pub section2_addr: u64,
}

impl UnpackedPackage2 {
    pub fn new(section0_addr: u64, section1_addr: u64, section2_addr: u64) -> Self {
        Self {
            section0_addr,
            section1_addr,
            section2_addr,
        }
    }
}

macro_rules! slice_to_word {
    ($inp:expr, $base:literal) => {
        u32::from_le_bytes([
            $inp[$base + 0],
            $inp[$base + 1],
            $inp[$base + 2],
            $inp[$base + 3],
        ])
    };
}

pub struct ParsedCtr {
    pub pkg2_raw_size: u32,
    pub key_generation: u32,
}

impl ParsedCtr {
    pub fn new(ctr: &[u8]) -> Self {
        let word0 = slice_to_word!(ctr, 0x0);
        let word1 = slice_to_word!(ctr, 0x4);
        let word2 = slice_to_word!(ctr, 0x8);
        let word3 = slice_to_word!(ctr, 0xC);

        let pkg2_raw_size = word2 ^ word3 ^ word0;
        let key_generation = ((word1 ^ (word1 >> 16)) & 0xFF) ^ (word1 >> 24);

        Self {
            pkg2_raw_size,
            key_generation,
        }
    }
}

pub fn get_enc_pkg2_header() -> RawPackage2Header {
    clear_and_invalidate_cache_for_address_range(PKG2_ADDR, 0x200);
    dsb_ish!();

    let mut data: [u8; 0x200] = [0u8; 0x200];
    unsafe {
        core::ptr::copy(PKG2_ADDR as *const u8, data.as_mut_ptr(), 0x200);
        core::mem::transmute(data)
    }
}

pub fn parse_pkg2_header() -> Package2Header {
    let enc_header = get_enc_pkg2_header();

    let mut inbuffer = [0; 0x200];
    let abuffer = align_buffer(&mut inbuffer, 0x40, 0x100);
    let mut outbuffer = [0; 0x200];
    let aobuffer = align_buffer(&mut outbuffer, 0x40, 0x100);

    abuffer.copy_from_slice(&enc_header.encrypted_header);
    clear_and_invalidate_cache_for_address_range(abuffer.as_ptr() as u64, 0x100);
    clear_and_invalidate_cache_for_address_range(aobuffer.as_ptr() as u64, 0x100);
    dsb_ish!();

    do_pkg2_dec(aobuffer, abuffer, &enc_header.encrypted_header[0..0x10]);
    dsb_ish!();
    clear_and_invalidate_cache_for_address_range(aobuffer.as_ptr() as u64, 0x100);
    dsb_ish!();

    let mut dec_header: Package2Header = unsafe { core::mem::zeroed() };
    unsafe {
        core::ptr::copy(
            aobuffer.as_ptr(),
            &mut dec_header as *mut _ as *mut u8,
            0x100,
        );
    }

    dec_header
        .header_ctr
        .copy_from_slice(&enc_header.encrypted_header[0..0x10]);

    dec_header
}

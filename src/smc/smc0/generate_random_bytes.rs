use tx1_a57_common::{
    dsb_ish,
    hardware::security_engine,
    utils::{align_buffer, clear_and_invalidate_cache_for_address_range},
};

use crate::{hardware::mmu, smc::CallingRegisters};

pub fn smc0_generate_random_bytes(args: &mut CallingRegisters) {
    let size = args.x1;

    if size > 0x38 {
        args.x0 = 2;
        return;
    }

    args.x0 = 0;

    let mut buffer = [0u8; 0x80];
    let ab = align_buffer(&mut buffer, 0x40, 0x38);

    clear_and_invalidate_cache_for_address_range(ab.as_ptr() as u64, 0x40);
    dsb_ish!();
    security_engine::rng::generate_random(mmu::SE_BASE, mmu::TMR_BASE, mmu::UART_BASE, ab);
    dsb_ish!();
    clear_and_invalidate_cache_for_address_range(ab.as_ptr() as u64, 0x40);
    dsb_ish!();

    unsafe {
        core::ptr::copy(
            ab.as_ptr(),
            (&mut args.x1) as *mut _ as *mut u8,
            size as usize,
        );
    }
}

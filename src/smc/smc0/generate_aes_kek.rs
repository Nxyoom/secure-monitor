use tx1_a57_common::{hardware::security_engine, log, utils::align_buffer};

use crate::{hardware::mmu, smc::CallingRegisters};

const AES_KEK_GEN_SRC: [[u8; 0x10]; 4] = [
    [
        0x4d, 0x87, 0x09, 0x86, 0xc4, 0x5d, 0x20, 0x72, 0x2f, 0xba, 0x10, 0x53, 0xda, 0x92, 0xe8,
        0xa9,
    ],
    [
        0x25, 0x03, 0x31, 0xfb, 0x25, 0x26, 0x0b, 0x79, 0x8c, 0x80, 0xd2, 0x69, 0x98, 0xe2, 0x22,
        0x77,
    ],
    [
        0x76, 0x14, 0x1d, 0x34, 0x93, 0x2d, 0xe1, 0x84, 0x24, 0x7b, 0x66, 0x65, 0x55, 0x04, 0x65,
        0x81,
    ],
    [
        0xaf, 0x3d, 0xb7, 0xf3, 0x08, 0xa2, 0xd8, 0xa2, 0x08, 0xca, 0x18, 0xa8, 0x69, 0x46, 0xc9,
        0x0b,
    ],
];

const AES_KEK_GEN_KEYMASK_SRC: [[u8; 0x10]; 4] = [
    [
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00,
    ],
    [
        0xa2, 0xab, 0xbf, 0x9c, 0x92, 0x2f, 0xbb, 0xe3, 0x78, 0x79, 0x9b, 0xc0, 0xcc, 0xea, 0xa5,
        0x74,
    ],
    [
        0x57, 0xe2, 0xd9, 0x45, 0xe4, 0x92, 0xf4, 0xfd, 0xc3, 0xf9, 0x86, 0x38, 0x89, 0x78, 0x9f,
        0x3c,
    ],
    [
        0xe5, 0x4d, 0x9a, 0x02, 0xf0, 0x4f, 0x5f, 0xa8, 0xad, 0x76, 0x0a, 0xf6, 0x32, 0x95, 0x59,
        0xbb,
    ],
];

const AES_KEK_GEN_SRC_STAGE2: [[u8; 0x10]; 4] = [
    [
        0xf4, 0x0c, 0x16, 0x26, 0x0d, 0x46, 0x3b, 0xe0, 0x8c, 0x6a, 0x56, 0xe5, 0x82, 0xd4, 0x1b,
        0xf6,
    ],
    [
        0x7f, 0x54, 0x2c, 0x98, 0x1e, 0x54, 0x18, 0x3b, 0xba, 0x63, 0xbd, 0x4c, 0x13, 0x5b, 0xf1,
        0x06,
    ],
    [
        0xc7, 0x3f, 0x73, 0x60, 0xb7, 0xb9, 0x9d, 0x74, 0x0a, 0xf8, 0x35, 0x60, 0x1a, 0x18, 0x74,
        0x63,
    ],
    [
        0x0e, 0xe0, 0xc4, 0x33, 0x82, 0x66, 0xe8, 0x08, 0x39, 0x13, 0x41, 0x7d, 0x04, 0x64, 0x2b,
        0x6d,
    ],
];

pub fn generate_aes_kek(
    access_key: [u8; 0x10],
    master_key_revision: u32,
    is_key_personalised: bool,
    key_use: u32,
    keymask_idx: u32,
) -> Option<[u8; 0x10]> {
    if is_key_personalised && keymask_idx != 0 {
        return None;
    }

    if (key_use | master_key_revision | keymask_idx) > 3 {
        return None;
    }

    let mut wrapped_key: [u8; 0x10] = [0u8; 0x10];
    for (iter, x) in wrapped_key.iter_mut().enumerate() {
        *x = AES_KEK_GEN_KEYMASK_SRC[keymask_idx as usize][iter]
            ^ AES_KEK_GEN_SRC[key_use as usize][iter];
    }

    let keytable_src = match is_key_personalised {
        true => 0xd,
        false => security_engine::aes::keytable_slot_by_masterkey_revision(
            mmu::SE_BASE,
            mmu::TMR_BASE,
            mmu::UART_BASE,
            master_key_revision,
        ),
    };

    security_engine::aes::unwrap_key(
        mmu::SE_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
        9,
        keytable_src,
        &wrapped_key,
    );

    let mut buffer: [u8; 0x50] = [0; 0x50];
    let ab1 = align_buffer(&mut buffer, 0x40, 0x10);
    security_engine::aes::unwrap_key_to_memory(
        mmu::SE_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
        9,
        &access_key,
        ab1,
    );

    security_engine::aes::unwrap_key(
        mmu::SE_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
        9,
        0xa,
        &AES_KEK_GEN_SRC_STAGE2[keymask_idx as usize],
    );

    let mut buffer2: [u8; 0x50] = [0; 0x50];
    let ab2 = align_buffer(&mut buffer2, 0x40, 0x10);
    security_engine::aes::aes_operation(
        mmu::SE_BASE,
        mmu::TMR_BASE,
        mmu::UART_BASE,
        ab2,
        ab1,
        0,
        9,
    );
    Some(ab2.try_into().unwrap())
}

pub fn smc0_generate_aes_kek(args: &mut CallingRegisters) {
    let access_key_low = args.x1;
    let access_key_high = args.x2;
    let mut access_key: [u8; 0x10] = [0; 0x10];

    for (iter, (low, high)) in access_key_low
        .to_le_bytes()
        .into_iter()
        .zip(access_key_high.to_le_bytes().into_iter())
        .enumerate()
    {
        access_key[iter] = low;
        access_key[iter + 8] = high;
    }

    let master_key_revision = args.x3 as u32;
    let options = args.x4 as u32;

    if options > 0xff {
        args.x0 = 2;
        return;
    }

    let is_key_personalised = (options & 0b1) == 1;
    let key_use = (options >> 1) & 0xf;
    let keymask_idx = (options >> 5) & 0b111;

    match generate_aes_kek(
        access_key,
        master_key_revision,
        is_key_personalised,
        key_use,
        keymask_idx,
    ) {
        Some(buffer) => {
            args.x0 = 0;
            unsafe {
                core::ptr::copy(buffer.as_ptr(), &mut args.x1 as *mut _ as *mut u8, 0x10);
            }
        }
        None => {
            log::write(mmu::UART_BASE, "[Nxyoom] generate_aes_kek failed");
            args.x0 = 2;
        }
    }
}

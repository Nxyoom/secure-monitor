use tx1_a57_common::{
    hardware::{security_engine, tmr::full_system_reset_via_watchdog},
    log,
    utils::set_pmc_scratch_200_if_unset,
};

use crate::{
    hardware::{fuse, mmu},
    smc::CallingRegisters,
};

pub fn smc1_panic(args: &mut CallingRegisters) {
    log::write(mmu::UART_BASE, "[Nxyoom] Panic SMC Called\n");
    let colour = args.x1 as u32;
    let reg_colour = ((colour & 0xf0) | ((0xf & colour) << 8) | ((colour & 0xf00) >> 8)) << 20;
    set_pmc_scratch_200_if_unset(mmu::PMC_BASE, reg_colour | 0x40);

    security_engine::lock(mmu::SE_BASE);
    fuse::disable_regprogram(mmu::FUSE_BASE);
    full_system_reset_via_watchdog(mmu::TMR_BASE);
}

use crate::{
    hardware::{self, cpu::wake_up_cpu, mmu},
    smc::CallingRegisters,
};

pub fn smc1_start_cpu(args: &mut CallingRegisters) {
    let cpuid = args.x1;
    let entrypoint = args.x2;
    let ctx = args.x3;

    if cpuid > 3 {
        args.x0 = (-1i64) as u64;
        return;
    }

    // TODO: Ensure core isn't already awake, but as N forget to update the value it can't be that important lol

    args.x0 = 0;

    hardware::cpu::set_core_state(cpuid as u8, entrypoint, ctx);

    wake_up_cpu(mmu::TMR_BASE, cpuid as u8);
}

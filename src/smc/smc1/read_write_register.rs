use tx1_a57_common::{
    log,
    mmio::{self, mmio_read, mmio_write},
};

use crate::{hardware::mmu, smc::CallingRegisters};

const RW_REGISTER_WHITELIST: [u8; 0x28] = [
    0xb9, 0xf9, 0x07, 0x00, 0x00, 0x00, 0x80, 0x03, 0x00, 0x00, 0x00, 0x17, 0x00, 0xc4, 0x07, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x40, 0x00,
];

pub fn smc1_read_write_register(args: &mut CallingRegisters) {
    let address = args.x1;

    if address & 0b11 != 0 {
        args.x0 = 2;
        return;
    }

    let adjusted_to_pmc_base = address - mmio::pmc::PMC_BASE as u64;

    if adjusted_to_pmc_base > 0xBFF {
        args.x0 = 2;
        return;
    }

    let index_adjusted_to_pmc_base = adjusted_to_pmc_base / 4;

    let shifted_index = index_adjusted_to_pmc_base / 8;

    if shifted_index > 0x27
        || (RW_REGISTER_WHITELIST[shifted_index as usize] & (1 << (index_adjusted_to_pmc_base & 7)))
            == 0
    {
        log::write(
            mmu::UART_BASE,
            "[Nxyoom] smc1_read_write_register called with bad address",
        );
        args.x0 = 2;
        return;
    }

    let mask = args.x2 as u32;
    let input = args.x3 as u32;
    const BASE: usize = 0x18007b000;

    let original_value = match mask {
        u32::MAX => 0,
        _ => mmio_read(BASE, address as usize),
    };

    if mask != 0 {
        mmio_write(
            BASE,
            address as usize,
            (original_value & !mask) | (input & mask),
        );
    }

    args.x1 = original_value as u64;
    args.x0 = 0;
}

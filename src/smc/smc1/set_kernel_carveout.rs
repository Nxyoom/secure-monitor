use crate::{
    hardware::{memory_controller, mmu},
    smc::CallingRegisters,
};

pub fn smc1_set_kernel_carveout(args: &mut CallingRegisters) {
    let index = args.x1;
    let address = args.x2;
    let size = args.x3;

    if size & 0x1ffff != 0 || address & 0x1ffff != 0 || size > 0x1ffe0000 || index > 1 {
        args.x0 = 2;
        return;
    }

    unsafe {
        memory_controller::KERNEL_CARVEOUT_METADATA[index as usize] =
            memory_controller::KernelCarveoutMetadata::new(address, size)
    };

    memory_controller::update_carveouts_4_and_5(mmu::MEMORY_CONTROLLER_BASE);

    args.x0 = 0;
}

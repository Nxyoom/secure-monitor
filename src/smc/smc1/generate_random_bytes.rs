use tx1_a57_common::{
    dsb_ish,
    hardware::security_engine,
    utils::{align_buffer, clear_and_invalidate_cache_for_address_range},
};

use crate::{
    hardware::{self, mmu},
    smc::CallingRegisters,
};

pub fn smc1_generate_random_bytes(args: &mut CallingRegisters) {
    let size = args.x1;

    if size > 0x38 {
        args.x0 = 2;
        return;
    }

    if let Some(lock) = super::super::USERMODE_SMC_RUNNING.try_lock() {
        let mut buffer = [0u8; 0x80];
        let ab = align_buffer(&mut buffer, 0x40, 0x38);

        clear_and_invalidate_cache_for_address_range(ab.as_ptr() as u64, 0x40);
        dsb_ish!();
        security_engine::rng::generate_random(mmu::SE_BASE, mmu::TMR_BASE, mmu::UART_BASE, ab);
        dsb_ish!();
        clear_and_invalidate_cache_for_address_range(ab.as_ptr() as u64, 0x40);
        dsb_ish!();

        unsafe {
            core::ptr::copy(
                ab.as_ptr(),
                (&mut args.x1) as *mut _ as *mut u8,
                size as usize,
            );
        }

        hardware::security_engine::refill_rng_buffer(
            mmu::SE_BASE,
            mmu::TMR_BASE,
            mmu::UART_BASE,
            false,
        );
        drop(lock);
    } else {
        let mut buffer = [0u8; 0x38];
        if !hardware::security_engine::get_from_rng_buffer(&mut buffer[..size as usize]) {
            args.x0 = 2;
            return;
        };
        unsafe {
            core::ptr::copy(
                buffer.as_ptr(),
                (&mut args.x1) as *mut _ as *mut u8,
                size as usize,
            );
        }
    }

    args.x0 = 0;
}

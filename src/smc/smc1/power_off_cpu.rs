use crate::{hardware::cpu, smc::CallingRegisters};

pub fn smc1_power_off_cpu(_args: &mut CallingRegisters) {
    cpu::power_off_core();
}

# Nxyoom Secure Monitor

A work-in-progress Rust reimplementation of the Nintendo Switch Secure Monitor for Firmware version 3.0.2.

## Running

You can build a binary with `cargo build-bin`

This should be loaded on a core in the A57 cluster @ `0x4002d000`

## Credits

* [referenced rboot's MMU implementation](https://github.com/Thog/rboot/blob/master/src/mmu.rs)
* Switchbrew
